from dashboard import Dashboard

bucket = 'policypalacnprod'
prefix = 'agero/prod/datapipeline/dynamodb-backup/prod-driveraggregate/2017-08-09-09-29-36/'
out_bucket = 'policypalacnprod'
out_key = 'agero/prod/datapipeline/dynamodb-backup/prod-driveraggregate/2017-08-09-09-29-36-output/'

def handler():
    d = Dashboard(bucket, prefix, out_bucket, out_key)
    d.dashboard()
    d.process_data()

handler()
